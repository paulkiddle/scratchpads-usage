let queue = Promise.resolve();

module.exports = async function(callback){
	const $$ = global.$$;
	$$.async();

	try {
		// Wait for any existing callback to run, ignore errors
		await queue.catch(()=>{});

		// Queue this callback and wait for it
		const o = await (queue = callback($$));
		$$.sendResult(o);
	} catch(e) {
		$$.sendError(e);
	}
}
