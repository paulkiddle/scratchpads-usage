function htmlEntities(str) {
	return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
}

function translate(...args){
	return `translate(${args.join(',')})`;
}

const html = (strings, ...values) => html.raw(
		strings.reduce((a, s, ix) => {
			let v = values[ix-1];
			if(v.toHtml) {
				v = v.toHtml();
			} else if(Array.isArray(v)) {
				v = v.join('');
			} else if(!html.isHtml(v)){
				v = htmlEntities(v);
			}

			return a + v + s
		})
	);

class Html extends String {
	toHtml() { return this; }
	_toHtml() { return this; }
}
html.raw = v => new Html(v);
html.isHtml = v => v instanceof Html;

class Table {
	constructor(headers, rows){
		this.headers = headers;
		this.rows = rows;
	}

	get toHtml(){
		return this._toHtml;
	}

	_toHtml() {
		const title = h => h.title ? h.title : h;

		return html`<div style="width:100%;max-height:700px;overflow:auto;"><table>
			<thead>
				<tr>${this.headers.map(h => html`<th>${title(h)}</th>`)}</tr>
			</thead>
			<tbody>
			${this.rows.map((r) => html`
				<tr>${this.headers.map((h, ix) => html`<td>${r[h.key||ix]}</td>`)}</tr>
			`)}
			</tbody>
		</table></div>`;
	}
}

class Svg {
	constructor(children, attrs) {
		this.children = children;
		this.attrs = attrs;
	}

	_toSvg() {
		const attrs = Array.from(Object.entries(this.attrs), ([k, v]) => html` ${k}="${v}"`);
		return html`<svg${attrs}>${this.children}</svg>`;
	}
}

class BottomAxis {
	constructor(scale, attrs = {}){
		this.scale = scale;
		this.attrs = attrs;
	}

	_toSvg() {
		const attrs = Array.from(Object.entries(this.attrs), ([k, v]) => html` ${k}="${v}"`);
		return html`<svg${attrs}>${this.toHtml()}</svg>`;
	}

	toHtml() {
		const attrs = Array.from(Object.entries(this.attrs), ([k, v]) => html` ${k}="${v}"`);
		const range = this.scale.range();
		const format = this.scale.tickFormat(null,'%b %Y');

		return html`<g${attrs}>
			<path fill="none" stroke="currentColor" d="M0.5,6V0.5H${range[1] + 0.5}V6"></path>
			${this.scale.ticks().map(date =>
				html`<g transform="translate(${this.scale(date) + 0.5},0)">
						<line stroke="currentColor" y2="6"></line>
						<text y="9" dy="0.71em" transform="rotate(30)">${format(date)}</text>
				</g>`
			)}
			</g>`;
	}
}


class LeftAxis {
	constructor(scale, attrs = {}){
		this.scale = scale;
		this.attrs = attrs;
	}

	_toSvg() {
		const attrs = Array.from(Object.entries(this.attrs), ([k, v]) => html` ${k}="${v}"`);
		return html`<svg${attrs}>${this.toHtml()}</svg>`;
	}

	toHtml() {
		const attrs = Array.from(Object.entries(this.attrs), ([k, v]) => html` ${k}="${v}"`);
		const range = this.scale.range();
		const format = this.scale.tickFormat();

		return html`<g${attrs}>
			<path fill="none" stroke="currentColor" d="M0,0.5H6V${range[0] + 0.5}H0" transform="translate(12, 0)"></path>
			${this.scale.ticks().map(value =>
				html`<g transform="translate(0, ${this.scale(value) + 0.5})">
						<line stroke="currentColor" x1="12" x2="18"></line>
						<text text-anchor="end" x="8" y="6">${format(value)}</text>
				</g>`
			)}
			</g>`;
	}
}

function g(attrs = {}, children = []) {
	attrs = Array.from(Object.entries(attrs), ([k, v]) => html` ${k}="${v}"`);
	return html`<g${attrs}>${children.map(c => c.toHtml())}</g>`
}

class Line {
	constructor(line, data) {
		this.line = line;
		this.data = data;
	}
	toHtml(){
		return html`<path fill="none" stroke="currentColor" d="${this.line(this.data)}" stroke-linecap="round" />`;
	}
}

function lineGraph({ x, y }, lines) {
	let labels = [];
	if(!Array.isArray(lines)) {
		const keys = Object.keys(lines);
		labels = keys.map(key => {
			const line = lines[key];
			const node = line[line.length-1];
			return html`<text x="${x(node[0])}" y="${y(node[1])}">${key}</text>`
		});
		lines = keys.map(k => lines[k]);
	}

	const line = d3.line().curve(d3.curveStepAfter).x(d => x(d[0])).y(d => y(d[1]));

	return new Svg(
		g({transform: translate(36, 18)},
			[
				(new BottomAxis(x, { transform: translate(18, y(1)) })).toHtml(),
				(new LeftAxis(y, { })).toHtml(),
				g({transform: translate(18, 0)},
					[
						...lines.map(data => new Line(line, data)),
						...labels
					]
				)
			]
		),
		{ height: 700, width: 1500 }
	)._toSvg();
}

module.exports = { Table, html, BottomAxis, Svg, translate, Line, LeftAxis, g, lineGraph };
