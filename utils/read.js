const fs = require('fs').promises;
const parseFile = require('./parse');

async function readFile(path, name) {
	if(readFile.cache[path]) {
		return readFile.cache[path];
	}

	return readFile.cache[path] = parseFile(await fs.readFile(path, 'utf8'), name);
}
readFile.cache = {};

async function* readFiles(){
	yield await readFile('./data/bio.acousti.ca', 'bio.acousti.ca');

	for(const file of await fs.readdir('./data/get.scratchpads.eu')) {
		yield await readFile('./data/get.scratchpads.eu/'+file, file);
	}
}

module.exports = readFiles;
