function counter(init = { count: 0 }, nameKey = 'name') {
	const dict = {};

	return {
		count(name, value) {
			if(!dict[name]) {
				dict[name] = {
					[nameKey]: name,
					...init
				}
			}

			if(!value) {
				return;
			} else if(typeof value !== 'object') {
				value = { count: Number(value) }
			}

			for(const [ key, increment ] of Object.entries(value)) {
				dict[name][key] += increment;
			}
		},
		get dict() {
			return dict;
		},
		get values() {
			return Object.values(dict);
		}
	}
}

module.exports = {
	run: require('./async'),
	getSites: require('./read'),
	display: require('./render'),
	counter
}
