const { unserialize } = require('php-serialize');

/**
 * Parse an entire file into its contents
 * @param {String} contents File contents
 */
function parseFile(contents, name) {
	const data = { name };
	const matches = contents.matchAll(/__(table|total|serialized):([a-z0-9._]+)__\n(.*?)(?=\n__|$)/gs);
	for(const [_,type,name,output] of matches) {
		const keys = name.split('.');
		const key = keys.pop();
		const o = keys.reduce((o, k)=>(o[k]=o[k]||{}), data);

		switch(type) {
			case 'table':
				o[key] = parseTable(output);
				break;
			case 'total':
				o[key] = parseTotal(output);
				break;
			case 'serialized':
				o[key] = parseSerialized(output);
				break;
			default:
				throw new TypeError(`Can't handle output type ${type}.`);
		}
	}
	return data;
}

function parseTotal(str){
	const m = str.match(/\d+/);
	return m && Number(m[0]);
}

function parseTable(str) {
	if(!str) {
		return [];
	}
	const [l0, ...lines] = str.trim().split('\n');
	const keys = l0.split('\t').map(k => k.replace('(*)', ''));

	return lines.map(
		line => Object.fromEntries(
			line.split('\t').map(
				(value, ix) => [keys[ix], value]
			)
		)
	);
}

function parseSerialized(str) {
	if(!str) {
		return null;
	}

	return unserialize(str.split('\n')[1]);
}

module.exports = parseFile;
