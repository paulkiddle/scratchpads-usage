# Feature Evaluation

What are the features of scratchpads, how are they used?

Written for jupyter labs with the Node JS kernel (IJavascript).

Requires Node 12.

Collect data using ./collect/collect.sh

## What constitutes a feature?

There are two main parts to a feature: data, and tools.

Primarily, Scratchpad sites are for organising data; they're a data repository.
Many of the features in Scratchpads add new data models.

However, Scratchpads are also tools to generate, modify, and present data and metadata; whether that is
import from an external service, discussing the data in a forum or blog, creating relationships, or exporting data.

## How to detect features

Scratchpads features are provided by drupal modules.
The highest level overview we can achieve is by analysing the modules enabled on each site.
Those modules that are enabled across many sites sites are candidates for deeper analasys and
potentially inform the focus of a move away from the current platform.
Modules that appear on few sites are candidates for deprecation. The sites they are used on
may have to migrate to a more specialist platform, or lose the features that these modules were providing.

Furthermore, there are a number of features that scratchpads maintainers can enable or disable at will, known as tools. These are immediate candidates for deeper analasys: what functionality to they
provide, and how are they actually used? These modules may be enabled but unused, alternatively they may be disabled but the data they introduced is still in use.

## All modules
Collect data on enabled modules

## Tools
Analasys of each tool, and any data collected.
