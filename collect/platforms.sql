--Get the list of sites from get.scratchpads.eu along with their status and platform
select node.title, s.status, p.title from hosting_site as s left join node on (node.nid=s.nid) left join node as p on (s.platform=p.nid) where s.status >-1;
