# Citations

We can record citations using `find`:

```bash
find /var/www/cite.scratchpads.eu/ -name *.pdf | grep '[^\/]\+/20[0-9]\{2\}-[01][0-9]-[0-3][0-9]/.*'
```

This has not been added to the script yet
