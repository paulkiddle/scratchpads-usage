DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROUT="/tmp/output"
getp=$(base64 -w0 $DIR/get-platforms.sql)
getsite=$(base64 -w0 $DIR/../get.sql)
echo Generate usage files... >&2
ssh -t sp-control-01 "
	mkdir $ROUT
	echo $getsite | base64 -d | sudo -u aegir drush @$1 sqlc --extra=-f > $ROUT/$1"
echo Download usage files... >&2
rsync --progress sp-control-01:$ROUT/$1 $DIR/../../data/get.scratchpads.eu/$1
