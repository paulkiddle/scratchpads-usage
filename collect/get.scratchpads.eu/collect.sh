DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROUT="/tmp/output"
getp=$(base64 -w0 $DIR/get-platforms.sql)
getsite=$(base64 -w0 $DIR/../get.sql)
echo Generate usage files... >&2
ssh -t sp-control-01 "
	mkdir $ROUT
	rm $ROUT/*
	echo $getp | base64 -d | sudo -u aegir drush @hm sqlc --extra=-N | while read line
	do
		echo $getsite | base64 -d | sudo -u aegir drush @\$line sqlc --extra=-f > $ROUT/\$line
	done
	tar -czf $ROUT.tar.gz -C $ROUT ./
	rm -rf $ROUT"
echo Download usage files... >&2
rsync --progress sp-control-01:$ROUT.tar.gz .
