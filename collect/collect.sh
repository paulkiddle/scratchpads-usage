DIR="../data"
rm -r $DIR/*
./bio.acousti.ca/collect.sh > $DIR/bio.acousti.ca
mkdir $DIR/get.scratchpads.eu
./get.scratchpads.eu/collect.sh
tar -xf output.tar.gz -C $DIR/get.scratchpads.eu
rm output.tar.gz
