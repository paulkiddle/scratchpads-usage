DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
getsite=$(base64 -w0 $DIR/../get.sql)
echo Generate usage file... >&2
ssh -t sp-bio-01 "
	cd /var/lib
	echo $getsite | base64 -d | sudo docker exec -i scratchpads.apache drush sqlc --extra=-f > /tmp/usage.txt" >&2
echo Download usage file... >&2
ssh sp-bio-01 "cat /tmp/usage.txt"
