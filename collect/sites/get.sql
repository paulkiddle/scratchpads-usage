--  All modules available and status
select name, status from system;

-- All nodes
select "";
select type, count(*) from node group by type;


--  bhl: created dates
select "__module:bhl__" as ``;
select created from cache_bhl;

--  biography
--  To count users that have filled in any of the extra fields:
select "__module:biography__" as ``;
select count(*) from (select entity_id from field_data_field_alternative_name
	union select entity_id from field_data_field_areas_of_professional_inte
	union select entity_id from field_data_field_biography
	union select entity_id from field_data_field_birth_date
	union select entity_id from field_data_field_birth_place
	union select entity_id from field_data_field_comments
	union select entity_id from field_data_field_death_date
	union select entity_id from field_data_field_example_of_handwriting
	union select entity_id from field_data_field_gender
	union select entity_id from field_data_field_known_for
	union select entity_id from field_data_field_main_collaborators_co_auth
	union select entity_id from field_data_field_make_this_profile_public_
	union select entity_id from field_data_field_orcid
	union select entity_id from field_data_field_places_worked
	union select entity_id from field_data_field_related_links
	union select entity_id from field_data_field_standard_form
	union select entity_id from field_data_field_user_keywords) as t1;

--  To count only users who have made their profiles public:
select count(*) from field_data_field_make_this_profile_public_ where field_make_this_profile_public__value=1;

--  Blogs created & updated dates
select "__module:blog__" as ``;
select created, changed from node where type='blog';

-- Number of views with charts
select "__module:charts__" as ``;
select count(*) from views_display where display_options like '%"style_plugin";s:6:"charts"%';

-- Language, status, created and changed dates of translated content
select "__module:translation__" as ``;
select language, status, created, changed from entity_translation;

-- Number of contexts created
select "__module:contexts__" as ``;
select count(*) from context;

-- Number of type, status, created and changed dates of darwincore nodes
select "__module:dwc__" as ``;
select type, status, created, changed from node where type in ('location', 'specimen_observation');

-- Number of dwcarchives
select "__module:dwcarchiver__" as ``;
select count(*) from dwcarchiver_archive;

-- Ecoint nodes
select "__module:ecoint__" as ``;
select status, created, changed from node where type='ecological_interactions';

-- Eolapi searches: created, changed dates
select "__module:eolapi__" as ``;
select created, changed from eolapi where type='search';

-- Event nodes
select "__module:event__" as ``;
select status, created, changed from node where type='event';

-- Exif
select "__module:exif__" as ``;
-- Number of field mappings
select count(*) from exif_custom_maps as e left join exif_custom_mapped_fields as f on(f.mid = e.mid) where img_field != '0';
-- Is global default set?
select count(*) from variable where name like 'exif_custom_default';
-- Number of user settings
select count(*) from exif_custom_users;

-- Forum topics creation date, last comment date, number of comments
select "__module:forum__" as ``;
select created, last_comment_timestamp, comment_count from forum_index;

-- Number of gbif map layers
select "__module:gbifmap__" as ``;
select count(*) from eol_gbif_maps_tid_map;

-- Is the site registered with gbif?
select "__module:gbifclient__" as ``;
select count(*) from variable where name like 'scratchpads_gbif_registry_client_uuid';

-- Number of groups
select "__module:groups__" as ``;
select state, created from og;

-- Group content including entity, group id, state, created date
select "__module:groupcontent__" as ``;
select entity_type, gid, state, created from og_membership;

-- IUCN taxon results: creation date and status (1=no data)
select "__module:iucn__" as ``;
select created, data like 'The IUCN does not hold any information for%' as no_data from cache_iucn;

-- Lexicon: Number of vocabularies used as lexicons
select "__module:lexicon__" as ``;
select value from variable where name like 'lexicon_vids';

-- Lucid: Number of lucid archives
select "__module:lucid__" as ``;
select value from variable where name like 'scratchpads_lucid_archives';

-- NCBI: Created dates of linked taxa
select "__module:ncbi__" as ``;
select created from cache_ncbi;

-- Newsletter
select "__module:newsletter__" as ``;
-- Number of categories
select count(*) from simplenews_category;
-- Created dates
select created from simplenews_subscriber;
-- Nodes
select status, created, changed from node where type = 'simplenews';

-- Pensoft publications
select "__module:pensoft__" as ``;
select created, changed, status, published from publication;

-- ReFindIt created dates of linked taxa
select "__module:refindit__" as ``;
select created from cache_refindit;

-- Rules: Status and count of rules configs
select "__module:rules__" as ``;
select status, count(*) from rules_config group by status;

-- UI Translate: Language and number of translations
select "__module:translateui__" as ``;
select language, count(*) from locales_target group by language;

-- Views: Number of custom views
select "__module:views__" as ``;
select count(*) from views_view;

-- Webforms
select "__module:webforms__" as ``;
-- Nodes
select created, changed, status from node where type='webform';
-- Submissions draft status & submitted date
select is_draft, submitted from webform_submissions;

-- Worms - number of linked taxa
select "__module:worms__" as ``;
select count(*) from worms;
