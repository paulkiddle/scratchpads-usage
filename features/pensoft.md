# Taxonomic paper (Pensoft writing tool)

Module: pensoft_publication

Creates the publication entity type.

 drush sqlq "select created, changed, status, published from publication;"
