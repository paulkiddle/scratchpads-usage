# Rules

Module: rules
Module: rules_ui

Set up actions based on certain conditions. The `rules` is actually not very useful for users without `rules_ui`,
so perhaps the former should be removed from the tools menu.

Rule configurations are added as a rules_config entity to the rules_config table. Status 1 is enabled, 0 is disabled.

drush sqlq "select status, count(*) from rules_config group by status;"
