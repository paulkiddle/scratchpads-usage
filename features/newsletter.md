# Simplenews

Module: simplenews

Creates a newsletter node type and notifies subscribers by email when a node of that type is created.

Count categories:

drush sqlq "select count(*) from simplenews_category;"

Get subscribers & creation date:

drush sqlq "select created from simplenews_subscriber;"

Get simplenews nodes:

drush sqlq "select * from node where type = 'simplenews';"
