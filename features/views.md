# Views

Module: views_ui

Enables the user interface for the `views` module, enabling users to customise the default views.

New or modified views are added to the `views_view` table.

drush sqlq "select count(*) from views_view;"
