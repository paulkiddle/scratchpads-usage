Blogs

module: scratchpads_blog

Creates a new node type 'blog'

Count total number of blog posts against time of creation and update:

drush sqlq "select created, changed from node where type='blog'"
