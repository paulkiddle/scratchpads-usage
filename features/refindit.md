# Refindit Widget

Module: refindit

Fetches data from ReFindIt to display on taxon pages. Caches data in the cache_refindit table.

drush sqlq "select created from cache_refindit;"
