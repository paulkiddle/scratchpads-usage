# Darwincore

Module: darwincore

Creates location and specimen_observation node types.

drush sqlq "select type, status, created, changed from node where type in ('location', 'specimen_observation');"
