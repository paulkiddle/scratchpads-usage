GBIF Maps

module: eol_gbif_maps

Displays a GBIF distrbution overlay on taxonomy maps pages, creating link between taxon & gbif backbone

Count total number of eol_gbif_maps_tid_map records:

drush sqlq "select count(*) from eol_gbif_maps_tid_map"
