# Webforms

module: webform

Creates a webform node type and records user responses.

# Nodes
drush sqlq "select created, changed, status from node where type='webform';"
# Submissions draft status & submitted date
drush sqlq "select is_draft, submitted from webform_submissions;"
