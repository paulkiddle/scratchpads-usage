# Content Translate

Module: scratchpads_translate

Allows users to translate contents of entities to different languages.

Check number of translated entities, dates created and updated, languages etc in the entity_translation table.

drush sqlq "select language, status, created, changed from entity_translation;"
