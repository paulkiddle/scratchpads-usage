# Cite this page

Module: scratchpads_citethispage

Enables creating citations for sites, with cite.scratchpads.eu as the backend.

`find /var/www/cite.scratchpads.eu/ -name *.pdf | grep '[^\/]\+/20[0-9]\{2\}-[01][0-9]-[0-3][0-9]/.*'`
