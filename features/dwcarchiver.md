# DarwinCore Archiver (DwC-A)

Module: dwcarchiver

Creates entries in the `dwcarchiver_archive` table representing each dwc archive the user has created (not including hard-coded archives provided by hooks such as the gbif extension).

drush sqlq "select count(*) from dwcarchiver_archive"
