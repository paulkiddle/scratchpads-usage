	  "bhl",
[bigmenu](modules.md) [contrib] - Replaces large menus with AJAX for efficiency
[context_ui](context.md) [contrib]
[darwincore](darwincore.md) [custom]
[dictionary_export](modules.md) [custom] - Allows a Microsoft Word formatted custom dictionary to be exported from a vocabulary
[dwcarchiver](dwcarchiver.md) [custom]
[ecoint](ecoint.md) [custom]
[eol_gbif_maps](gbif_maps.md) [custom]
	  "eolapi",
[exif_custom](exif.md) [custom]
[femail](modules.md) [custom] - Email interface for creating/reading forum posts & replies
[gm3_filter](modules.md) [custom] - Filter for rendering maps in bodies of text
[googleanalytics](modules.md) [contrib] - Adds google analytics trackers to pages
	  "iucn",
	  "ncbi",
[pensoft_publication](pensoft.md) [custom]
	  "refindit",
[rules](rules.md) [contrib]
	  "rules_admin",
[scratchpads_biblio_search_page_number](modules.md) [custom] - Adds page number to biblio search results
[scratchpads_biography](biography.md) [custom]
[scratchpads_blog](blogs.md) [custom]
	  "scratchpads_charts",
[scratchpads_citethispage](cite.md) [custom]
[scratchpads_classification_service_client](modules.md) [custom]
[scratchpads_col](modules.md) [custom] - This is actually not a useful module and [should be removed](https://github.com/NaturalHistoryMuseum/scratchpads2/issues/6164) from all sites
[scratchpads_events](events.md) [custom]
[scratchpads_forum](forum.md) [custom]
[scratchpads_gbif_registry_client](gbif.md) [custom]
[scratchpads_group](groups.md) [custom]
[scratchpads_lexicon](lexicon.md) [custom]
[scratchpads_lucid_applet](lucid.md) [custom]
[scratchpads_multilingual](ui-translate.md) [custom]
[scratchpads_multilingual_contribute](modules.md) [custom] - Enables the translate-in-page feature. Also syncs translations to the scratchpads localize server, though this service is currently not running.
[scratchpads_pretty_urls](modules.md) [custom] - Creates pretty URLs for entities based on their title
[scratchpads_sharethis](modules.md) [custom] - Adds social media sharing tools to pages
[scratchpads_show_taxa_revisions](modules.md) [custom] - Show taxonomy revision editor and revision date on each taxonomy overview tab
[scratchpads_sitemap](modules.md) [custom] - Uses the `site_map` module to display a site map
[scratchpads_thumbnail_style_alter](modules.md) [custom] - Allows setting thumbnails to rectangular dimensions
[scratchpads_translate](content-translate.md) [custom]
[silvercsv](modules.md) [custom] - Enables CSV import in `silver` module
[simplenews](newsletter.md) [contrib]
[taxon_stats](modules.md) [custom] - Adds taxon statistics, very similar to taxonomystatistics
[taxonomystatistics](modules.md) [custom] - Adds a taxonomy statistics page, which shows statistics about taxonomies
[tcs_url](modules.md) [custom] Adds URL field to the tcsdc import form
[tracker](modules.md) [core]	Adds a page that lists contributions by users
	  "views_ui",
	  "webform",
[worms](worms.md) [custom]


Disabled:
		"character_editor",
	  "collections_profile",
	  "dwca_export",
