# NCBI Widget

Module: ncbi

Fetches data from NCBI to display on taxon pages. Caches data in the cache_ncbi table.

drush sqlq "select created from cache_ncbi;"
