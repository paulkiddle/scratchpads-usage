# Interface Translate

Module: scratchpads_multilingual

Allows users to translate elements of the user interface into different languages.

We can't tell which translations have been added by the user as opposed to provided by other sources, but we can count the total number of translations for each language.

drush sqlq "select language, count(*) from locales_target group by language;"
