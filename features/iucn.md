# IUCN Widget

Module: iucn

Fetches data from IUCN to display on taxon pages. Caches data in the cache_iucn table.

drush sqlq "select created, data like 'The IUCN does not hold any information for%' as no_data from cache_iucn;"
