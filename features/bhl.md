# BHL Widget

Module: bhl

Fetches data from the BHL to display on taxon pages. Caches data in the cache_bhl table.

select created from cache_bhl;
