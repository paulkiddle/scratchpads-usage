# Groups

Module: scratchpads_group

Enables the `og` module, which creates groups for users and content to be associated with.

Table `og` records the list of groups and creation dates, table `og_membership` records users and content associated with each group.

# Number of groups
drush sqlq "select state, created from og;"

# Group content including entity, group id, state, created date
drush sqlq "select entity_type, gid, state, created from og_membership;"
