# Forum

Module: scratchpads_forum

Enables the `forum` module, which creates a forum node type and `forum_index` table for tracking the forum topics, created dates and last comment date.

drush sqlq "select created, last_comment_timestamp, comment_count from forum_index;"
