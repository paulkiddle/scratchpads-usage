# Custom EXIF mapping

Module: exif_custom

Creates a mapping to automatically populate image entity fields based on exif fields present on the image file.

Are maps present?
drush sqlq "select count(*) from exif_custom_maps as e left join exif_custom_mapped_fields as f on(f.mid = e.mid) where img_field != '0';"

Is there a global default?
drush sqlq "select count(*) from variable where name like 'exif_custom_default'"

Are there custom user settings?
drush sqlq "select count(*) from exif_custom_users;"
