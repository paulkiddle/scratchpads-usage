# Lucid Applet

Module: scratchpads_lucid_applet

Allows embedding the Lucid 3 java applet in order to view an uploaded lucid file.

Embeded files are added to the scratchpads_lucid_archives variable.

drush sqlq "select value from variable where name like 'scratchpads_lucid_archives'" | php -r 'echo count(unserialize(fgets(STDIN))) . "\n";'
