# GBIF Registry Client

Module: scratchpads_gbif_registry_client

Generates a DWCArchive and submits site to GBIF.
Records created record's uuid in a variable.

drush sqlq "select * from variable where name like 'scratchpads_gbif_registry_client_uuid'"
