# Views Charts

Module: scratchpads_charts

Enables modules that allow the use of graphs in views. This includes the `views_charts` module, which creates the `charts` style plugin.

select count(*) from views_display where display_options like '%"style_plugin";s:6:"charts"%';
