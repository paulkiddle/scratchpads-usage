# Biography

Module: scratchpads_biography

Add additional user fields that can be used to show a public biography of a person.

To count users that have filled in any of the extra fields:
	select count(*) from (select entity_id from field_data_field_alternative_name
	union select entity_id from field_data_field_areas_of_professional_inte
	union select entity_id from field_data_field_biography
	union select entity_id from field_data_field_birth_date
	union select entity_id from field_data_field_birth_place
	union select entity_id from field_data_field_comments
	union select entity_id from field_data_field_death_date
	union select entity_id from field_data_field_example_of_handwriting
	union select entity_id from field_data_field_gender
	union select entity_id from field_data_field_known_for
	union select entity_id from field_data_field_main_collaborators_co_auth
	union select entity_id from field_data_field_make_this_profile_public_
	union select entity_id from field_data_field_orcid
	union select entity_id from field_data_field_places_worked
	union select entity_id from field_data_field_related_links
	union select entity_id from field_data_field_standard_form
	union select entity_id from field_data_field_user_keywords) as t1;


To count only users who have made their profiles public:
  select count(*) from field_data_field_make_this_profile_public_ where field_make_this_profile_public__value=1;
