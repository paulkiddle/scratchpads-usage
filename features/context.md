Context layouts

module: context_ui

Enables a UI for the context module, allowing users to create contexts.

Count the total number of contexts that have been created.

drush sqlq 'select count(*) from context'
