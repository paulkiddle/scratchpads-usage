# WoRMS Importer

Module: worms

Creates entries in the `worms` table to link an import to a taxonomy.

drush sqlq "select count(*) from worms"
