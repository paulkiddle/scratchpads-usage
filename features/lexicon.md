# Lexicon

Module: scratchpads_lexicon

Enables the lexicon module, `lexicon_vids`, which adds the ability to use taxonomies as lexicons.

The vocabularies used as lexicon terms are stored in lexicon_vids, and can be counted:

drush sqlq "select value from variable where name like 'lexicon_vids'" | php -r 'echo count(array_filter(unserialize(fgets(STDIN)))) . "\n";'
