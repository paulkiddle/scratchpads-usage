

## Get modules considered tools
module_load_include('inc', 'tools', 'tools.admin');
array_keys(tools_get_features());

Include 3 disabled tools:
 - "character_editor",
 - "collections_profile",
 - "dwca_export",


## Check list of enabled modules

drush sqlq "select name, status from system where name in ($tools)"
